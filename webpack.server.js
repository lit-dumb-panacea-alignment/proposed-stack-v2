const path = require('path');
const nodeExternals = require('webpack-node-externals');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const serverConfig = {
  entry: {
    app: './src/server/server.tsx',
  },
  plugins: [
    new CleanWebpackPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: [
          'style-loader',
          'css-loader'
        ],
      },
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
      },
      // {
      //   test: /\.(png|jpg)$/,
      //   loader: 'url-loader'
      // }
    ],
  },
  target: 'node',
  externals: [
    nodeExternals({})
  ],
  resolve: {
    extensions: [ '.tsx', '.ts', '.js', '.css', 'json' ],
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist/server'),
  },
  mode: 'development',
  devtool: 'inline-source-map'
};

module.exports = serverConfig;