'use strict';

const Webpack = require('webpack');
const axios = require('axios')
const path = require('path')
const webpackConfig = require('./webpack.server');
const compiler = Webpack(webpackConfig);

const app = require('express')();

const server = require('http').createServer(app);
const io = require('socket.io')(server, {
  cors: {
    origin: "http://localhost:3000",
    methods: ["GET", "POST"]
  }
});

io.on('connection', () => {
  console.log('connection TO DEV SERVER')
});

server.listen(4000, function () {
  console.log(`*** DEV SERVER *** listening on 4000`);
});

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const watcher = compiler.watch({
  ignored: path.join(__dirname, 'dist/client/*')
}, async function(err, stats) {
  console.log('___ SERVER RECOMPILED AND BUNDLED ___')
  // console.log(stats.toString({ colors: true }))

  await sleep(1000)

  try {
    console.log('*** emitting ***')
    io.emit('fileChanged', 'asdfadsf');
    // await axios.get('http://localhost:3000/reload')
  } catch(e) {
    // console.error('error reloading')
  }
})



