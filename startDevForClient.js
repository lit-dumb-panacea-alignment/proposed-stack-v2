'use strict';

const Webpack = require('webpack')
const axios = require('axios')
const open = require('open')
const webpackConfig = require('./webpack.client')

const compiler = Webpack(webpackConfig);

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const watcher = compiler.watch({}, async function(err, stats) {
  console.log('___ CLIENT RECOMPILED AND BUNDLED ___')
  console.log(stats.toString({ colors: true }))
})



