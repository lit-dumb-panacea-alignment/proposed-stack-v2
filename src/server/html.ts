/**
 * Html
 * This Html.ts file acts as a template that we insert all our generated
 * application code into before sending it to the client as regular HTML.
 * Note we're returning a template string from this function.
 */
const html = ({
  body,
  title,
  styles,
  clientJavascriptBundle
}: {
  body: string,
  title: string,
  styles: string,
  clientJavascriptBundle: string
}) => `
  <!DOCTYPE html>
  <html>
    <head>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>${title}</title>
      ${styles}
    </head>
    <body>
    <div id="app">${body}</div>
    <script type="text/javascript" src="${clientJavascriptBundle}"></script>
    
  </body>
</html>
`;

export default html;