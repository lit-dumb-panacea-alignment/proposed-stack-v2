import express from 'express';
import * as React from 'react';
import { renderToString } from 'react-dom/server';
import App from '../components/App';
import http from 'http'
import cors from 'cors'
import fs from 'fs'
import manifest from '../../dist/client/manifest.json'

import { StaticRouter } from "react-router-dom";

import { ServerStyleSheet } from 'styled-components'

import html from './html';

type manifestObject = { [key: string]: string }

const port = 3000;
const server = express();

server.use(cors())

let httpServer = new http.Server(server);
// set up socket.io and bind it to our
// http server.
let io = require("socket.io")(httpServer);

server.use(express.static('dist/client'));

server.get('*', (req, res) => {
  let staticContext: any = {}

  const sheet = new ServerStyleSheet()

  const body = renderToString(sheet.collectStyles(
    <StaticRouter location={req.url} context={staticContext}>
      <App />
    </StaticRouter>
  ))
  const styles = sheet.getStyleTags()
  const title = 'Server side Rendering with Styled Componentss!';

  // const clientJavascriptBundle: string = (manifest as manifestObject)['client.js']
  const clientJavascriptBundle: string = 'client.js'

  res.status(staticContext.statusCode || 200).send(
    html({
      title,
      styles,
      body,
      clientJavascriptBundle
    })
  );
});

// whenever a user connects on port 3000 via
// a websocket, log that a user has connected
io.on("connection", function (socket: any) {
  console.log("* connection *");

  socket.on('disconnect', function () {
    console.log('disconnected event');
    //socket.manager.onClientDisconnect(socket.id);
    socket.disconnect();
  });
});

io.on("closed", function (socket: any) {
  console.log('closing connection');
});

httpServer.listen(port, function () {
  console.log(`listening on ${port}`);
});

// fs.watch('', function (curr, prev) {
  // file changed push this info to client.
  // console.log("file Changed");
  // io.emit('fileChanged', 'yea file has been changed.');
// });