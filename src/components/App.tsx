import * as React from 'react';
import testImage from '../assets/test.png'
import { Hello } from './Hello';
import styled from 'styled-components';
import { Normalize } from 'styled-normalize'
import { Link, Route, Switch } from 'react-router-dom';
import { Home } from './Home';
import { Nav } from './Nav';
import { SideDrawer } from './SideDrawer';
import { Backdrop } from './Backdrop';
import GlobalStyle from './globalStyles';

const AppContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  width: 100%;
  height: 100%;
  font-size: 40px;
  background: linear-gradient(20deg, rgb(219, 112, 147), #daa357);
`;

function RouteStatus(props: any) {
  return (
    <Route
      render={({ staticContext }) => {
        // we have to check if staticContext exists
        // because it will be undefined if rendered through a BrowserRouter
        // console.log('props', props)
        // console.log('staticContext', staticContext)

        if (staticContext) {
          staticContext.statusCode = props.statusCode;
        }

        return <div>{props.children}</div>;
      }}
    />
  );
}

function NotFound() {
  return (
    <RouteStatus code={404}>
      <div>
        <h1>Sorry, can’t find that.</h1>
      </div>
    </RouteStatus>
  );
}

function About() {
  return (
    <RouteStatus code={200}>
      <div>
        <h1>Edge1234567</h1>
        <Link to="dashboard">To Dashboard</Link>
      </div>
    </RouteStatus>
  );
}

function Dashboard() {
  return (
    <RouteStatus code={200}>
      <div>
        <h1>Dashboard</h1>
        <Link to="about">To About</Link>
      </div>
    </RouteStatus>
  );
}

const App = (props: any) => {
  const [open, setOpen] = React.useState(false)

  return (
    <>
      <Normalize />
      <GlobalStyle />
      <Nav onClick={() => setOpen(!open)} />
      <SideDrawer show={open} />
      {
        open &&
        <Backdrop onClick={() => setOpen(false)} />
      }
      <main style={{
        // backgroundColor: 'red',
        height: '100%'
      }}>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/about" component={About} />
          <Route path="/dashboard" component={Dashboard} />
          <Route component={NotFound} />
        </Switch>
      </main>
    </>
  );
}

export default App;