import * as React from "react";
import {
	FaItunesNote
} from 'react-icons/fa';

import {
	BiCubeAlt,
	BiImageAlt
} from "react-icons/bi";

import {
	Toolbar,
	Navigation,
	Logo,
	Links,
	Items,
	ListItem,
	Spacer,
	LogoBlock
} from './styles'

import DrawerToggleButton from './DrawerToggleButton'

interface NavProps {
	onClick: () => void
}

export function Nav(props: NavProps) {

	return (
		<Toolbar>
			<Navigation>
				<div>
					<DrawerToggleButton onClick={props.onClick} />
				</div>
				<LogoBlock>
					<Logo href="/">
						{/* <BiImageAlt /> */}
						W
					</Logo>
				</LogoBlock>
				<Spacer />
				<div>
					<Items>
						<ListItem>
							<Links>
								<FaItunesNote />
							</Links>
						</ListItem>
						<ListItem>
							<Links><BiCubeAlt /></Links>
						</ListItem>
					</Items>
				</div>
			</Navigation>
		</Toolbar>
	)
}