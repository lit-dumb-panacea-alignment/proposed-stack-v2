import * as React from "react";
import { ReactElement, useState } from 'react';

export interface AboutProps {

}

export function About(props: AboutProps) {
	const [count, setCount] = useState(0)

	console.log('_!_!_')

	return (
		<div>
			<h1>About!</h1>
			<button onClick={() => setCount(count + 1)}>INC</button>
			<button onClick={() => setCount(count - 1)}>DEC</button>
			<p>{count}</p>
		</div>
	)
}