import * as React from "react";
// import { Link } from 'react-router-dom';
import {
  List,
  Nav,
  Link,
  ListItem
} from './styles'

interface Props {
  show: boolean
}

export function SideDrawer(props: Props) {

  return (
    <Nav show={props.show}>
      <List>
        <ListItem>
          <Link href="/">Products</Link>
        </ListItem>
        <ListItem>
          <Link href="/">Users</Link>
        </ListItem>
      </List>
    </Nav>
  )
}