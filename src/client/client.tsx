import React, { ReactElement } from 'react';
import ReactDOM from 'react-dom';
import App from '../components/App';
import { BrowserRouter } from "react-router-dom";
import { io } from 'socket.io-client';

console.log('123333$$$33')

window.onload = () => {
  ReactDOM.hydrate(
    <BrowserRouter>
      <App />
    </BrowserRouter>, document.getElementById('app')
  );

  const socket = io("http://localhost:4000");

  socket.on('connect', onConnect);

  socket.on('fileChanged', function (msg: string) {
    document.location.reload();
  });

  function onConnect() {
    console.log('connect ' + socket.id);
  }
};