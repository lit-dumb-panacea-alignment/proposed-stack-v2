const path = require('path');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const clientConfig = {
  context: __dirname,
  mode: 'development',
  devtool: 'inline-source-map',
  plugins: [
    new CleanWebpackPlugin({
      verbose: true
    }),
    new WebpackManifestPlugin({
      publicPath: '',
      // writeToFileEmit: true,
      // isInitial: true
    })
  ],
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: [
          'style-loader', 
          'css-loader'
        ],
      },
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.html$/i,
        loader: 'html-loader',
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
      },
      // {
      //   test: /\.(png|jpg)$/,
      //   loader: 'url-loader'
      // }
    ],
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js', '.css', 'html' ],
  },
  entry: {
    client: './src/client/client.tsx'
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist/client'),
  }
};

module.exports = clientConfig